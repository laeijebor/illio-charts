/**
 * Author: Leo Ijebor
 */

import React, { useState } from "react";
import {Text, View} from "react-native";
import { render, fireEvent} from "react-native-testing-library";

import BrandedInput from "../../src/components/BrandedInput";

const TestInput = ({Comp, onEventMock}) => {
	const [text, setText] = useState("emptyTextInput")
	return (
		<View>
			<Comp
				testID={"inputComponent"}
				value={text}
				onChange={(val) => {
					setText(val);
					onEventMock(val);
				}}
				label={"Text Input"}/>
		</View>
	)
}

describe("Rendering Inputs", () => {
	
	// Example of testing a Component. Expectations should be based around what we want the end component to be, not the implementation detail
	it("Renders BrandedInput.Text", () => {
		const onEventMock = jest.fn();
		const comp = render(
			<TestInput Comp={BrandedInput.Text} onEventMock={onEventMock}/>
		);
		
		const {getByTestId} = comp;
		const textInput = getByTestId("inputComponent");
		
		fireEvent(textInput, "onChange", "Hello");
		
		expect(onEventMock).toHaveBeenCalledWith("Hello");
		expect(comp.toJSON()).toMatchSnapshot("textInputWithTextHello");
		
	})
	
	
	
	
	
	it("Renders BrandedInput.Switch", () => {
		const onEventMock = jest.fn();
		let comp;
		
		comp = render(
			<TestInput Comp={BrandedInput.Switch} onEventMock={onEventMock}/>
		);
		const {getByTestId} = comp;
		let compJSON = JSON.stringify(comp.toJSON());
		
		expect(compJSON).toMatchSnapshot("Switch Before Toggle")
		
		const switchInput = getByTestId("inputComponent");
		fireEvent(switchInput, "onChange", true);
		expect(onEventMock).toHaveBeenCalledWith(true);
		let compJSONAfter = JSON.stringify(comp.toJSON());
		expect(compJSONAfter).toMatchSnapshot("Switch Before Toggle")
		
		
		fireEvent(switchInput, "onChange", false);
		expect(onEventMock).toHaveBeenCalledWith(false);
		expect(compJSON).toMatchSnapshot("Switch Before Toggle")
	})
	
})
