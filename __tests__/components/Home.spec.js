import React, {useState} from "react";

import {Text, View} from "react-native";

import { act, render, fireEvent} from "react-native-testing-library";

import BrandedInput from "../../src/components/BrandedInput";
import Home from "../../src/screens/Home";

jest.mock("../../src/components/AssetChart", () => "View");


jest.mock("../../src/components/BrandedInput", () => ({
	Text: "BrandedTextInput",
	Switch: "BrandedSwitch",
	DatePicker: "BrandedTextInput"
}));


fetch.mockResponse(JSON.stringify([
	{
		date: "2020-01-02",
		open: 296.24,
		high: 300.6,
		low: 295.19,
		close: 300.35,
		adjusted_close: 299.6389,
		volume: 33911864
	},
	{
		date: "2020-01-03",
		open: 297.15,
		high: 300.58,
		low: 296.5,
		close: 297.43,
		adjusted_close: 296.7258,
		volume: 36633878
	},
	{
		date: "2020-01-06",
		open: 293.79,
		high: 299.96,
		low: 292.75,
		close: 299.8,
		adjusted_close: 299.0902,
		volume: 29644644
	}
]))

describe("Home App", () => {
	beforeEach(() => {
		fetch.resetMocks()
	})
	it("Renders the screen", () => {
		const screen = render(<Home />);
		
		const { getByTestId } = screen;
		
		const from = getByTestId("fromDate");
		const to = getByTestId("toDate");
		
		const fromDate = new Date(2019,10,1);
		const toDate = new Date(2019,10,30);
		
		
		act(() => {
			fireEvent(from, "onChange", fromDate)
			fireEvent(to, "onChange", toDate)
		});
		
		
		expect(fetch.mock.calls[1][0]).toEqual("https://eodhistoricaldata.com/api/eod/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d.&from=2019-10-1&to=2019-10-30&fmt=json");
		
		expect(screen.toJSON()).toMatchSnapshot("Home Screen");
		
	})
})
