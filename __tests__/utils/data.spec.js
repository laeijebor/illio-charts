/**
 * Author: Leo Ijebor
 */

import { fetchClosingPrices } from "../../src/utils/data";

describe("Testing the API", () => {
	beforeEach(() => {
		fetch.resetMocks()
	})
	it("should generate a suitable url", async () => {
		
		const expectedCalls = [ [ 'https://eodhistoricaldata.com/api/eod/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d.&from=2020-0-1&to=2020-0-2&fmt=json' ] ]
		fetch.mockOnce()
		await fetchClosingPrices("AAPL.US", new Date(2020, 0, 1), new Date(2020, 0, 2));
		console.log(fetch.mock.calls);
		expect(fetch.mock.calls[0][0]).toEqual(expectedCalls[0][0]);
	});
})

