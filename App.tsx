import React, {useEffect, useState} from 'react';

import * as Font from "expo-font";

import MainNavigator from "./src/navigation/MainNavigator";
import Background    from "./src/components/Background";
import BrandedText   from "./src/components/BrandedText";

const cacheFonts = async () => {
    await Font.loadAsync({
        Roboto: require("./assets/fonts/Roboto/Roboto-Regular.ttf"),
    });
};

export default function App() {
    const [ready, setReady] = useState(false);
    
    useEffect(() => {
        
        cacheFonts().then(() => setReady(true))
        setReady(true);
        
        
    }, []);
    if (!ready) return <Background><BrandedText>Loading fonts...</BrandedText></Background>
    return (
        <MainNavigator/>
    );
}
