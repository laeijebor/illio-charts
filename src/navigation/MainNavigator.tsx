import React from "react";

import {Button} from "react-native";

import {createStackNavigator} from "@react-navigation/stack";
import {NavigationContainer}  from "@react-navigation/native";

import Home from "../screens/Home";
import Info from "../screens/Info";

const Stack = createStackNavigator();

const MainNavigator = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name={"Home"}
                    component={Home}
                />
                <Stack.Screen
                    name={"Info"}
                    component={Info}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default MainNavigator;
