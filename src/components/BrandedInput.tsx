/**
 * Author: Leo Ijebor
 * Some simple reusable and consistenly styled components for text and date entry
 */

import BrandedText                                      from "./BrandedText";
import React                                            from "react";
import {StyleSheet, Switch, TextInput, View, YellowBox} from "react-native";
import DatePicker                                       from 'react-native-datepicker';
/**
 * TODO: Choose between react-native-datepicker and @react-native-community/datetimepicker and remove dependency on other
 */

// react-native-datepicker not yet updated to deal with DatePickerIOS merge with Android...
YellowBox.ignoreWarnings(["DatePickerIOS has been merged with"])


import {Colors} from "../constants/Colors";

const Wrapper = ({children, label}) => {
    return (
        <>
            <BrandedText style={[styles.label, styles.indent]}>{label}</BrandedText>
            {children}
        </>
    )
}

/**
 * BrandedTextInput, BrandedDatePicker both take the same props
 *
 * @param value: The text / datethat is to be displayed
 * @param onChange: Function that takes a string / date, to be called when the component changes
 * @param label: Label to show above the text
 * @param placeholder: Placeholder to display, where applicable
 */
interface BrandedInputProps {
    label?: string;
    placeholder?: string;
    testID?: string;
    onChange([params]: any): void; //
}

interface BrandedTextInputProps extends BrandedInputProps {
    value: string;
}

interface BrandedDatePickerProps extends BrandedInputProps {
    value: Date;
}

interface BrandedSwitchProps extends BrandedInputProps {
    value: boolean
}


const BrandedTextInput = ({value, onChange, label, ...rest}: BrandedTextInputProps): JSX.Element => {
    return (
        <Wrapper label={label}>
            <TextInput
                style={styles.input}
                value={value}
                onChangeText={value => onChange(value)}
                placeholderTextColor={Colors.TEXT_PRIMARY}
                {...rest}
            />
        </Wrapper>
    )
}


const BrandedDatePicker = ({value, onChange, label}: BrandedDatePickerProps): JSX.Element => {
    return (
        <Wrapper label={label}>
            <DatePicker
                date={value}
                style={[styles.noGaps, styles.dateInput]}
                onDateChange={(date) => {
                    onChange(date)
                }}
                mode="date"
                placeholder="select date"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                customStyles={{
                    dateInput: [styles.noBorder, styles.dateInput],
                    dateText:  styles.dateText,
                }}
            />
        </Wrapper>
    )
}

const BrandedSwitch = ({value, onChange, label}: BrandedSwitchProps): JSX.Element => {
    return (
        <View style={[styles.horizontal, styles.topPadding]}>
            <BrandedText style={styles.label}>{label}</BrandedText>
            <Switch
                trackColor={{false: "#767577", true: Colors.LINES}}
                thumbColor={value ? "white" : Colors.LINES}
                onValueChange={onChange}
                value={value}
            />
        </View>
    )
}

const BORDER_WIDTH = 2;
const SHADING_COLOR = Colors.BACKGROUND_LIGHT;

const styles = StyleSheet.create({
    dateInput:  {
        alignItems:      "flex-start",
        justifyContent:  "center",
        backgroundColor: SHADING_COLOR,
    },
    dateText:   {
        color:             Colors.TEXT_PRIMARY,
        fontSize:          20,
        textAlign:         "left",
        paddingHorizontal: 15,
    },
    horizontal: {
        flexDirection:  "row",
        justifyContent: "space-between",
        alignItems:     "center",
    },
    indent:     {
        paddingLeft: 6,
    },
    input:      {
        color:             Colors.TEXT_PRIMARY,
        fontSize:          20,
        borderColor:       Colors.LINES,
        borderWidth:       BORDER_WIDTH,
        lineHeight:        25,
        padding:           5,
        paddingHorizontal: 15,
        height:            40,
        width:             "100%",
        backgroundColor:   SHADING_COLOR,
    },
    label:      {
        fontWeight:    "500",
        fontSize:      16,
        paddingTop:    10,
        paddingBottom: 2,
    },
    noBorder:   {
        borderWidth: 0,
        borderColor: "transparent",
    },
    noGaps:     {
        borderWidth:    BORDER_WIDTH,
        borderColor:    Colors.LINES,
        margin:         0,
        padding:        0,
        width:          "100%",
        alignItems:     "flex-start",
        justifyContent: "flex-start",
    },
    topPadding: {
        paddingTop: 20,
    }
});


export default {
    Text:       BrandedTextInput,
    DatePicker: BrandedDatePicker,
    Switch:     BrandedSwitch,
}
