import React                         from "react";
import {StyleSheet, Text, TextStyle} from "react-native";
import {Colors}                      from "../constants/Colors";

interface BrandedTextProps {
    children?: string | JSX.Element | Array<string | JSX.Element>;
    style?: TextStyle | TextStyle[];
}

const BrandedText = ({children, style, ...rest}: BrandedTextProps): JSX.Element => {
    
    return (
        <Text style={[defaultStyles.text, style]} {...rest}>{children}</Text>
    )
}

const defaultStyles = StyleSheet.create({
    text: {
        color: Colors.TEXT_PRIMARY
    }
})
export default BrandedText;
