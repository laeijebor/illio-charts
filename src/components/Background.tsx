/**
 * Author: Leo Ijebor
 * Ordinarily I'd use a themed Background component to ensure consistency of layout, look and feel.
 * This implementation is a little redundant as it's not reused, but including to show you how I'd typically work.
 *
 */

import React, {ReactNode} from "react";

import {StyleSheet, View} from "react-native";
import {Colors}           from "../constants/Colors";


interface IBackgroundProps {
    children?: ReactNode;
}


const Background = ({children}: IBackgroundProps) => {
    return (
        <View style={styles.container}>
            {children}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:            1,
        backgroundColor: Colors.BACKGROUND,
    },
});

export default Background
