/**
 * Here I'm using the render props pattern, so that we could swap out the visual implementation, independently of the functionality.
 * This can be useful in cases where we'd:
 * - Like to use different visual implementations depending on the context / device (e.g. larger for iPad devices)
 * - Change maintain the functionality implementation, but change the visual elements (e.g. if we wanted to move the label around, we'd have good separation between the controller and the view)
 *
 * This is contrived and overkill in such a simple app (especially as we're not really providing much functionality!), but again trying to imagine this is part of a wider app
 */


import React                   from "react";
import {Dimensions, YellowBox} from "react-native";

import {VictoryAxis, VictoryLine, VictoryChart} from "victory-native";
import {Colors}                                 from "../constants/Colors";


/**
 * !Note: victory-native package has not updated for componentWillReceiveProps deprecation, and has incomplete acceptable props for Domain (which don't agree with their documentation
 * TODO: replace victory-native with a more up-to-date charting library, or consider submitting PR to improve TypeScript declaration and remove componentWillRecceiveProps
 */
YellowBox.ignoreWarnings(["Warning: Failed prop", "Warning: componentWillReceiveProps"])


export const DEFAULT_CHART_HEIGHT = 200;

// TODO: Should use localisation
const format = tick => {
    const d = new Date(tick);
    return `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear() % 100}`;
};

interface AssetChartProps {
    data: { date: Date, closePrice: number }[];
    max: number;
    height?: number;
    width?: number;
    truncate?: boolean;
}

const AssetChart = ({data, max, truncate = false, width = Dimensions.get("window").width - 70, height = DEFAULT_CHART_HEIGHT}: AssetChartProps) => {
    
    const yAxisProps: { domain } = {domain: null};
    if (!truncate) {
        yAxisProps.domain = [0, max]
    }
    
    return (
        <VictoryChart width={width} height={height}>
            <VictoryAxis
                orientation={"bottom"}
                label={"Date"}
                tickFormat={tick => format(tick)}
                style={{
                    axis:       {stroke: Colors.LINES},
                    tickLabels: {fill: "white"},
                    axisLabel:  {fill: "white"}
                }}
                fixLabelOverlap
            />
            
            <VictoryLine
                style={{
                    data:   {stroke: Colors.LINES},
                    labels: {fill: "red"}
                }}
                padding={50}
                data={data}
                interpolation={"basis"}
                animate={{
                    duration: 500,
                    onLoad:   {duration: 1000}
                }}
                x="date"
                y="closePrice"
            />
            <VictoryAxis
                style={{
                    axis:       {stroke: Colors.LINES},
                    tickLabels: {fill: "white"}
                }}
                dependentAxis
                orientation={"right"}
                label={"Close"}
                tickFormat={t => `$${Math.round(t * 100) / 100}`}
                {...yAxisProps}
            />
        </VictoryChart>
    )
}

export default AssetChart
