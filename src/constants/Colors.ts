/**
 * Short of proper theming, just some colors to keep the UI consistent
 */

export const Colors = {
    BACKGROUND: "#1F2334",
    BACKGROUND_LIGHT: "#292B43",
    BUTTONS: "#577CCF",
    LINES: "#6A72BE",
    TEXT_PRIMARY: "#AFB6D3"
}
