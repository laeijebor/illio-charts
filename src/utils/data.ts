/**
 * Author Leo Ijebor
 * Data fetching and manipulation helper functions
 */

interface ClosingPrice {
    date: string;
    open: number;
    high: number;
    low: number;
    close: number;
    adjusted_close: number;
    volume: number;
};

// TODO: Handle bad requests
export const fetchClosingPrices = async (ticker: string, from: Date, to: Date): Promise<any> => {
    const formatDate = (date) => `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`
    
    const earlier = from < to ? formatDate(from) : formatDate(to);
    const later = from > to ? formatDate(from) : formatDate(to);
    
    return await fetch(`https://eodhistoricaldata.com/api/eod/${ticker}?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d.&from=${earlier}&to=${later}&fmt=json`)
}
