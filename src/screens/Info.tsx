import BrandedText         from "../components/BrandedText";
import {View, StyleSheet,} from "react-native";
import React               from "react";
import {Colors}            from "../constants/Colors";
import Background          from "../components/Background";

interface BulletProps {
    text: string;
    cr?: boolean;
}

const Bullet = ({text}: BulletProps) => (<BrandedText>{'\u2022'}{text}</BrandedText>)

const Info = () => {
    return (
        <Background>
            <View style={styles.container}>
                <BrandedText style={styles.header}>
                    To be production ready:
                </BrandedText>
                <Bullet text={"Validate Ticker"}/>
                <Bullet text={"Check response status code and improve error messages"}/>
                <Bullet text={"Debounce requests"}/>
                <Bullet text={"Check fetch response ordering"}/>
                <Bullet text={"Validate json response"}/>
                <Bullet text={"E2E Tests using Detox or similar"}/>
                <Bullet text={"Allow for adjusted close prices"}/>
                <Bullet text={"Improve update policy for pushing OTA updates"}/>
            </View>
        </Background>
    )
};

const styles = StyleSheet.create({
    container: {
        padding:         20,
        backgroundColor: Colors.BACKGROUND_LIGHT,
        marginTop:       20,
    },
    header:    {
        fontWeight:    "700",
        paddingBottom: 10,
    },
})

export default Info;
