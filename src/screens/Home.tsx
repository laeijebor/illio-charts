/**
 * Author: Leo Ijebor
 * TODO: Describe roll of the homepage
 *
 */
import React, {useEffect, useReducer, useState} from "react";

import {ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";

import Background                         from "../components/Background";
import BrandedInput                       from "../components/BrandedInput";
import AssetChart, {DEFAULT_CHART_HEIGHT} from "../components/AssetChart";
import {Colors}                           from "../constants/Colors";
import BrandedText                        from "../components/BrandedText";
import {fetchClosingPrices}               from "../utils/data";

enum States {
    LOADING,
    ERROR,
    READY,
    NO_RESULTS,
}

const STATE_TEXT = {
    [States.LOADING]:    "Loading data",
    [States.ERROR]:      "Error getting data. Wrong ticker?",
    [States.NO_RESULTS]: "No results found",
};

/**
 * Initialisations
 */

const today = new Date();
const initialFrom = new Date();
initialFrom.setDate(today.getDate() - 28);

/**
 * REDUCER
 * A simple reducer. I would ordinarily put this as part of the redux reducers, but since I'm not using redux for this simple interface,
 * I'm just putting a demo reducer here. A little nod to the fact that you use Redux in your web apps ;-)
 */
const defaultSettings = {truncate: true};

enum SettingsActions {
    TOGGLE_TRUNCATE,
}

const settingsReducer = (state, action) => {
    switch (action.type) {
        case SettingsActions.TOGGLE_TRUNCATE:
            console.log("toggling from ", state);
            console.log("toggling to ", {
                ...state,
                truncate: !state.truncate,
            });
            return {
                ...state,
                truncate: !state.truncate,
            }
    }
    return state;
};

/** END REDUCER **/


/**
 * Home Component, incorporating the controls and chart
 */
const Home = ({navigation}): JSX.Element => {
    
    /**
     * LOCAL STATE
     * Note: If this was a redux app, I wouldn't use such local state
     */
    const [appState, setAppState] = useState(States.LOADING);
    const [ticker, setTicker] = useState("AAPL.US");
    const [from, setFrom] = useState(initialFrom);
    const [to, setTo] = useState(today);
    const [max, setMax] = useState(0);
    const [data, setData] = useState([]);
    const [settings, dispatch] = useReducer(settingsReducer, defaultSettings);
    
    /** REDUCER ACTIONS **/
    const toggleTruncation = () => {
        dispatch({type: SettingsActions.TOGGLE_TRUNCATE})
    };
    
    /**
     * EFFECTS
     * Fetch the data from the provided endpoint. Second argument causes side effect only to run once the ticker, from date, or to date have changed
     */
    useEffect(() => {
        (async function () {
            try {
                
                const response = await fetchClosingPrices(ticker, from, to);
                const json = await response.json();
                
                let newMax = 0;
                
                if (json.length === 0) {
                    setData([]);
                    setAppState(States.NO_RESULTS);
                    return;
                }
                
                // TODO: Could offer option to use adjusted close prices
                const newData = json.map(entry => {
                    if (entry.closePrice) newMax = Math.max(newMax, entry.closePrice);
                    return {date: new Date(entry.date), closePrice: entry.close};
                });
                
                setMax(newMax); // TODO: The fact that these are all being called at the same time suggests useReducer (or a Redux reducer) would be better suited
                setData(newData);
                setAppState(States.READY);
            } catch (err) {
                setAppState(States.ERROR);
            }
            
            
        })();
    }, [ticker, from, to]); // Make sure we only fetch when one of these changes
    
    
    /** RENDER OUTPUT **/
    return (
        <Background>
            <ScrollView style={styles.container}>
                <BrandedInput.Text value={ticker} onChange={setTicker} label={"ticker"} placeholder={"Enter ticker"}/>
                <BrandedInput.DatePicker
                    testID={"fromDate"}
                    value={from}
                    onChange={(date) => {
                        setFrom(new Date(date))
                    }} label={"from"}/>
                <BrandedInput.DatePicker
                    testID={"toDate"}
                    value={to}
                    onChange={(date) => {
                        setTo(new Date(date))
                    }} label={"to"}/>
                
                <View style={{height: 40}}/>
                
                <View style={styles.chart}>
                    {
                        appState === States.READY &&
						<AssetChart
							data={data}
							truncate={settings.truncate}
							max={max}
						/>
                    }
                    {
                        appState !== States.READY &&
						<BrandedText style={styles.placeHolderText}>{STATE_TEXT[appState]}</BrandedText>
                    }
                
                </View>
                
                <BrandedInput.Switch
                    label={"Allow y-axis truncation"}
                    value={settings.truncate}
                    onChange={toggleTruncation}
                />
                
                <TouchableOpacity onPress={() => {
                    navigation.navigate("Info")
                }}>
                    <BrandedText style={{marginTop: 20, paddingVertical: 10}}>Tap to see what's next</BrandedText>
                </TouchableOpacity>
            
            </ScrollView>
        </Background>
    )
};

const styles = StyleSheet.create({
    chart:     {
        alignItems:      "center",
        backgroundColor: Colors.BACKGROUND,
        borderColor:     Colors.LINES,
        borderWidth:     1,
        height:          DEFAULT_CHART_HEIGHT,
        justifyContent:  "center",
        shadowColor:     "white",
        shadowOffset:    {
            height: 0,
            width:  0,
        },
        shadowOpacity:   1,
        shadowRadius:    4,
    },
    container: {
        flex:    1,
        padding: 20,
    },
    
    placeHolderText: {
        fontSize: 18,
    },
});

export default Home
