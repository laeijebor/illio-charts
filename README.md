*Simple sample by Leo Ijebor. \
Approximately 5 or 6 hours work, on and off*

# Instructions

## Installing expo-cli
If you don't already use Expo for React Native, please install Expo CLI:

`yarn add expo-cli`

-- or --

`npm install -g expo-cli`


<br/><br/>
## Preparing to run the project

`npm install`

-- or --

`yarn install`


<br/><br/>
## Running on Android or iOS simulators



Firstly, start the Expo packager:

`yarn start`

-- or --

`npm start`

<br/>

### Android Emulator

Once the packager starts, follow the instruction to tap 'a' in the command line

<br/>

### iOS Emulator

Once the packager starts, follow the instruction to tap 'i' in the command line  
  
    
      
<br/>
          
### Physical Devices
Install Expo from the respective App Store, and follow the instructions on:  
https://docs.expo.io/versions/v36.0.0/get-started/installation/#2-mobile-app-expo-client-for-ios

<br/><br/>
# Testing
`yarn test` or `npm run test` to run jest suites once

`yarn testDebug` or `npm run testDebug` to run jest suites in watch mode


<br/><br/>
# Packages used
* **react-native-datepicker** as RN doesn't have built in cross-platform
* **react-navigation** and related packages to make the app feel a bit more "real"
* **redux**, as I know you tend to use it in your stack
* **victory-native** for the chart itself
* **jest, jest-expo, react-native-testing-library** for testing
